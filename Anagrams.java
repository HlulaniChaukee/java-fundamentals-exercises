import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;


public class Anagrams {
    public static void main(String[] args) {
        try {

            Scanner scan = new Scanner(System.in);

            System.out.println("Enter a word to search:");
            String key = scan.next();

            URL file = new URL("http://www.puzzlers.org/pub/wordlists/unixdict.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(file.openStream()));

            String move;
            int count = 0;

            while ((move = in.readLine()) != null)
            {

                    String sortedKey = matchCharacter(key);
                    String sortedMove = matchCharacter(move);

                    if(sortedKey.equalsIgnoreCase(sortedMove))
                    {
                        System.out.println(move);
                    }
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static String matchCharacter(String word)
    {
        char[] wordArray = word.toCharArray();
        Arrays.sort(wordArray);
        String sortedString = String.valueOf(wordArray);

        return sortedString;
    }

}
