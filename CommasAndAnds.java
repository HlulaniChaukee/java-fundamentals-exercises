import java.util.Scanner;

public class CommasAndAnds {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a word with brackets:");
        String word = scan.nextLine();

        String[] words = null;
        String message = "";

        if(word.equalsIgnoreCase("")){
            message = "{}";
        } else {
            words = word.split(" ");

            for (int x = 0; x < words.length; x++) {
                if (words.length == 1) {
                    message += words[x];

                } else if(x == 0) {
                    message += words[x].replace("[", "{").replace("\"", "");

                } else if (words.length > 1  && x < words.length - 1) {
                    message += words[x].replace("\"", " ");

                } else {
                    message += " and " + words[x].replace("\"", "").replace("]", "}");
                }
            }
        }
        message.replace("\"", " ");
        System.out.println(message);
    }
}