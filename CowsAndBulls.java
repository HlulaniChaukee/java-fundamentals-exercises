import java.util.Random;
import java.util.Scanner;

public class CowsAndBulls {
    public static void main(String[] args){
        Scanner add = new Scanner(System.in);

        int[] randomizedValues = randomNumbers();
        System.out.println("Please enter the 4 digit number to guess:");

        String gues = add.nextLine();
        int[] guessedValues = new int[gues.length()];

        for(int x=0;x < gues.length();x++){
            guessedValues[x] = Integer.parseInt(String.valueOf(gues.charAt(x)));
        }

        int bulls = 0, cows =0, option = 0;
        String guesedString = "", randomizeString = "", result = "";;

        for(int x = 0;x < guessedValues.length;x++) {
            guesedString += guessedValues[x];
            randomizeString += randomizedValues[x];
        }

        if(guesedString.equalsIgnoreCase(randomizeString)){
            result = "Congratulation You Have Won!";
        } else {
            for(int  x = 0;x < guessedValues.length;x++){
                if(guessedValues[x] == randomizedValues[x]){
                    bulls ++;
                } else {

                    for(int  y = 0;y < guessedValues.length;y++){
                        if(guessedValues[x] == randomizedValues[y]){
                            cows++;
                        }
                    }
                }

            }
            result = "You Have "+ bulls + " Bulls and  " + cows + " Cows";
            option = 1;
        }

        if(bulls == 0 && cows == 0 && guesedString.equalsIgnoreCase(randomizeString)) {
            result = "You Have lost";
        }
        String guesValue = "", ranValue = "";;

        for(int ran: randomizedValues){
            ranValue += ran;
        }
        System.out.println(" Random values are:\n" + ranValue);
        System.out.println(" " +  result);

    }
    /*Generating a random number */
    public static int randomValues(int maximum){

        Random value = new Random();
        int randomNum = value.nextInt(maximum);
        if(randomNum == 0){
            randomNum = 1;
        }
        return randomNum;
    }

    public static int[] randomNumbers(){
        int [] guesses = new int[4];
        int number = 0;

        for(int x = 0;x < guesses.length;x++) {
            number =  randomValues(10);
            if(x == 0){
                guesses[x]= number;

            } else {
                for(int y=0;y <= x;y++) {
                    if(number != guesses[y]) {
                        guesses[x]= number;
                    } else {
                        if(y ==0) {
                            while(number == guesses[y]){
                                number =  randomValues(10);
                            }
                        }

                        else if(y ==1){
                            while(number == guesses[y -1] || number == guesses[y]){
                                number =  randomValues(10);
                            }
                        } else if(y ==2){

                            while(number == guesses[y -2] || number == guesses[y -1] || number == guesses[y]) {
                                number =  randomValues(10);
                            }
                        } else {
                            while(number == guesses[y -3] || number == guesses[y -2] || number == guesses[y -1] || number == guesses[y]) {
                                number =  randomValues(10);
                            }
                        }

                        guesses[x]= number;
                    }
                }
            }


        }


        return guesses;
    }

}
