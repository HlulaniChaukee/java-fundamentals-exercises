/*
The program adds two integers.
It accepts two integer seperated by a space and print the sum.
*/

import java.util.Scanner;

public class AddTwoNumbers
{
    public static void main(String[] args)
    {

        Scanner add = new Scanner(System.in);
        int sum = 0;

        System.out.print("\rEnter two numbers:");
        int number1 = add.nextInt();
        int number2 = add.nextInt();

        sum = number1 + number2;
        System.out.print("the sum is:" + sum);

    }
}
