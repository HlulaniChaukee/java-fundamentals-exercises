
public class PancakeFlipping {
    public static void main(String [] args){

        int [] values = {4, 6, 8, 1 , 3, 10, 15};
        int temp = 0;

        System.out.println("Pancake values are:");
        for (int a = 0; a < values.length; a++){
            System.out.print(values[a]+ " ");
        }

        sortArray(values, temp);
    }

    public static void sortArray(int integers[], int move){

        for (int i = 0; i < (integers.length -1); i++) {
            for (int j = 0; j < (integers.length - i - 1); j++){

                if (integers[j] > integers[j + 1]) {
                    move = integers[j];
                    integers[j] = integers [j + 1];
                    integers[j + 1] = move;
                }
            }
        }
        System.out.println("\n\nSorted pancake values are:");
        for (int count = 0; count < integers.length; count++){
            System.out.print(integers[count]+ " ");
        }

    }
}