import java.util.*;

public class Sudoku {
    private class Spot implements Comparable<Spot> {

        private int row, col;
        private int value;
        private int part;

        private HashSet<Integer> possibleValues;

        Spot(int x, int y, int val){
            row = x;
            col = y;
            value = val;
            part = getPart(x, y);
            possibleValues = new HashSet<>();
        }

        Spot(Spot s) {
            this(s.row, s.col, s.value);
            part = s.part;
            possibleValues = new HashSet<>(s.possibleValues);
        }

        void setValue(int val) {
            value = val;
        }

        int getValue() {
            return value;
        }

        int getPartForSpot() {
            return part;
        }

        boolean isEmpty() {
            return value == 0;
        }

        HashSet<Integer> getPossibleValues() {
            if (value != 0) return null;

            /* temporarily assign all 9 numbers */
            for (int i = 1; i <= Sudoku.SIZE; i++)
                possibleValues.add(i);

            /* Remove all the values that cannot be placed at this Spot */
            possibleValues.removeAll(valInRows.get(row));
            possibleValues.removeAll(valInCols.get(col));
            possibleValues.removeAll(valInParts.get(part));
            return possibleValues;
        }

        public void updateValueInGrid() {
            solutionGrid[row][col].value = value;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        @Override
        public int compareTo(Spot that) {
            return this.possibleValues.size() - that.possibleValues.size();
        }

        @Override
        public boolean equals(Object o) {
            if (o == null) return false;
            if (!(o instanceof Spot)) return false;

            Spot that = (Spot) o;
            return this.row == that.row && this.col == that.col;
        }

        @Override
        public int hashCode() {
            return possibleValues.size() * 25;
        }

        @Override
        public String toString() {
            return value + "";
        }

        private int getPart(int x, int y) {
            if (x < 3) {
                if (y < 3) return PART1;
                else if (y < 6) return PART4;
                else return PART7;
            }
            if (x < 6) {
                if (y < 3) return PART2;
                else if (y < 6) return PART5;
                else return PART8;
            }
            else {
                if (y < 3) return PART3;
                else if (y < 6) return PART6;
                else return PART9;
            }
        }
    } // End of Spot class

    /* Member variables for the puzzle and solution grids */
    private Spot[][] puzzleGrid;
    private Spot[][] solutionGrid;

    /* List of all the possible solutions */
    private List<ArrayList<Spot>> solutions;
    private ArrayList<HashSet<Integer>> valInRows, valInCols, valInParts;
    private long timeTakenForSolution;

    /* Parts of the grid each of size 3x3. Counting from the
     * top left to top right then the next row below.
     * 0 1 2
     * 3 4 5
     * 6 7 8
     */
    private static final int PART1 = 0;
    private static final int PART2 = 1;
    private static final int PART3 = 2;
    private static final int PART4 = 3;
    private static final int PART5 = 4;
    private static final int PART6 = 5;
    private static final int PART7 = 6;
    private static final int PART8 = 7;
    private static final int PART9 = 8;

    // Provided easy 1 6 grid
    public static final int[][] easyGrid = Sudoku.stringsToGrid(
            "7 0 0  1 0 0  0 0 0",
            "0 2 0  0 0 0  0 1 5",
            "0 0 0  0 0 6  3 9 0",

            "2 0 0  0 1 8  0 0 0",
            "0 4 0  0 9 0  0 7 0",
            "0 0 0  7 5 0  0 0 3",

            "0 7 8  5 0 0  0 0 0",
            "5 6 0  0 0 0  0 4 0",
            "0 0 0  0 0 1  0 0 2");


    public static final int SIZE = 9;  // size of the whole 9x9 puzzle
    public static final int PART = 3;  // size of each 3x3 part
    public static final int MAX_SOLUTIONS = 100;

    public static int[][] stringsToGrid(String... rows) {
        int[][] result = new int[rows.length][];
        for (int row = 0; row<rows.length; row++) {
            result[row] = stringToInts(rows[row]);
        }
        return result;
    }

    public static int[][] textToGrid(String text) {
        int[] nums = stringToInts(text);
        if (nums.length != SIZE*SIZE) {
            throw new RuntimeException("Needed 81 numbers, but got:" + nums.length);
        }

        int[][] result = new int[SIZE][SIZE];
        int count = 0;
        for (int row = 0; row<SIZE; row++) {
            for (int col=0; col<SIZE; col++) {
                result[row][col] = nums[count];
                count++;
            }
        }
        return result;
    }

    public static int[] stringToInts(String string) {
        int[] a = new int[string.length()];
        int found = 0;
        for (int i=0; i<string.length(); i++) {
            if (Character.isDigit(string.charAt(i))) {
                a[found] = Integer.parseInt(string.substring(i, i+1));
                found++;
            }
        }
        int[] result = new int[found];
        System.arraycopy(a, 0, result, 0, found);
        return result;
    }
    public Sudoku(int[][] ints) {
        puzzleGrid = new Spot[SIZE][SIZE];
        solutionGrid = new Spot[SIZE][SIZE];

        solutions = new ArrayList<>();

        valInRows = new ArrayList<HashSet<Integer>>(SIZE);
        valInCols = new ArrayList<HashSet<Integer>>(SIZE);
        valInParts = new ArrayList<HashSet<Integer>>(SIZE);

        /* Initializing all the HashSets */
        for (int i = 0; i < SIZE; i++) {
            valInRows.add(new HashSet<Integer>());
            valInCols.add(new HashSet<Integer>());
            valInParts.add(new HashSet<Integer>());
        }

        /* Setting up the Sudoku puzzle grid with the appropriate Spots.
         * And adding all the values in the relevant Rows, Cols and Parts
         * to set up the initial state of the grid. */
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                int val = ints[i][j];
                Spot newSpot = new Spot(i, j, val);
                puzzleGrid[i][j] = newSpot;

                if (!newSpot.isEmpty()) {
                    valInRows.get(i).add(val);
                    valInCols.get(j).add(val);
                    valInParts.get(newSpot.getPartForSpot()).add(val);
                }
            }
        }
    }

    public Sudoku(String text) {
        this(Sudoku.textToGrid(text));
    }

    public int solve() {
        /* List of all the empty spots in the puzzleGrid */
        ArrayList<Spot> emptySpots = getEmptySpotsList();

        /* List of Spots that will hold the solution values for the puzzleGrid */
        ArrayList<Spot> solvedSpots = new ArrayList<>();

        long startTime = System.currentTimeMillis();

        solveSudoku(emptySpots, solvedSpots, 0);

        long endTime = System.currentTimeMillis();
        timeTakenForSolution = endTime - startTime;

        if (solutions.size() == 0) {
            return 0;
        }

        fillSolutionGrid();
        return solutions.size();
    }

    private void solveSudoku(ArrayList<Spot> emptySpots,
                             ArrayList<Spot> solvedSpots, int index) {

        if (solutions.size() >= Sudoku.MAX_SOLUTIONS)
            return;

        if (index >= emptySpots.size()) {
            solutions.add(new ArrayList<>(solvedSpots));
            return;
        }

        Spot currentSpot = new Spot(emptySpots.get(index));

        for (int value : currentSpot.possibleValues) {

            if (valueIsValid(value, currentSpot)){

                currentSpot.setValue(value);

                updateGridStateWithValue(value, currentSpot);

                solvedSpots.add(currentSpot);

                int newIndex = index + 1;

                solveSudoku(emptySpots, solvedSpots, newIndex);

                /* Backtrack when the method above returns */
                emptySpots.get(index).setValue(0);

                solvedSpots.remove(currentSpot);

                updateGridStateWithValue(value, currentSpot);
            }
        }
    }

    private void fillSolutionGrid() {
        ArrayList<Spot> solvedSpots = solutions.get(0);

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                solutionGrid[i][j] = new Spot(puzzleGrid[i][j]);
            }
        }

        for (Spot spot : solvedSpots) {
            spot.updateValueInGrid();
        }
    }
    private boolean valueIsValid(int value, Spot currentSpot) {
        int row = currentSpot.getRow();
        int col = currentSpot.getCol();
        int part = currentSpot.getPartForSpot();

        return (!valInRows.get(row).contains(value)) &&
                (!valInCols.get(col).contains(value)) &&
                (!valInParts.get(part).contains(value));
    }


    private void updateGridStateWithValue(int value, Spot currentSpot) {
        HashSet<Integer> valsInCurrentRow = valInRows.get(currentSpot.getRow());
        HashSet<Integer> valsInCurrentCol = valInCols.get(currentSpot.getCol());
        HashSet<Integer> valsInCurrentPart = valInParts.get(currentSpot.getPartForSpot());

        if (valsInCurrentRow.contains(value))
            valsInCurrentRow.remove(value);
        else
            valsInCurrentRow.add(value);

        if (valsInCurrentCol.contains(value))
            valsInCurrentCol.remove(value);
        else
            valsInCurrentCol.add(value);

        if (valsInCurrentPart.contains(value))
            valsInCurrentPart.remove(value);
        else
            valsInCurrentPart.add(value);

    }

    private ArrayList<Spot> getEmptySpotsList() {
        ArrayList<Spot> result = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                Spot thisSpot = puzzleGrid[i][j];
                if (thisSpot.isEmpty()) {
                    thisSpot.getPossibleValues();
                    result.add(thisSpot);
                }
            }
        }
        Collections.sort(result);
        return result;
    }

    public String getSolutionText() {
        if (solutions.size() == 0) return "No Solutions";
        String result = "";
        for (Spot[] sArr : solutionGrid) {
            result += "[";
            for (Spot s : sArr)
                result += s.toString() + ", ";
            result += "] \n";
        }
        return result;
    }

    public long getElapsed() {
        return timeTakenForSolution;
    }

    @Override
    public String toString() {
        String result = "";
        for (Spot[] sArr : puzzleGrid) {
            result += "[";
            for (Spot s : sArr)
                result += s.toString() + ", ";
            result += "] \n";
        }
        return result;
    }

    public static void main(String[] args) {
        Sudoku sudoku;
        sudoku = new Sudoku(easyGrid);

        System.out.println(sudoku); // print the raw problem
        int count = sudoku.solve();
        System.out.println("solutions:");
        //System.out.println("elapsed:" + sudoku.getElapsed() + "ms");
        System.out.println(sudoku.getSolutionText());
    }
}