import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Compress {

    private static final String INPUT_FILE = "C:\\Users\\RALSON\\IdeaProjects\\hluly\\src\\testFile.txt";
    private static final String OUTPUT_FILE = "C:\\Users\\RALSON\\IdeaProjects\\hluly\\src\\testFile.zip";

    public static void main(String[] args){
        compressing(new File(INPUT_FILE), OUTPUT_FILE);
    }

    public static void compressing(File inputFile, String FilePath){

        try {
            FileOutputStream myFile = new FileOutputStream(FilePath);
            ZipOutputStream displayMassege = new ZipOutputStream(myFile);

            ZipEntry zipEntry = new ZipEntry(inputFile.getName());
            displayMassege.putNextEntry(zipEntry);
            FileInputStream fileInputStream = new FileInputStream(inputFile);
            byte[] buf = new byte[1024];
            int bytesRead;

            // Read the input file by chucks of 1024 bytes and write the read bytes to the file stream
            while ((bytesRead = fileInputStream.read(buf)) > 0){
                displayMassege.write(buf, 0, bytesRead);
            }
            displayMassege.closeEntry();
            System.out.println("Regular file :\n" + inputFile.getCanonicalPath()+" is zipped to archive "+FilePath);

        } catch (IOException er) { er.printStackTrace(); }
    }
}