/**
 *This program accept digits from the user.
 * The program halt and print the sum when the enter the value (-1).
 */

import java.util.Scanner;

public class SumOfDigits {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int digit;

        System.out.println("Enter a positive digit");
        digit = input.nextInt();

        if (digit <= 0) {
            System.out.println("Incorrect digit");
        } else {
           int sum = 0;

            while (digit != 0){
                sum = sum+(digit%10);
                digit = digit/10;
            }
            System.out.println("sum is :" +sum);
        }
    }
}