/*
* This program ask the user to enter the total number of integers to sort.
* The progeam uses bubble sort algorithm to sort in ascending and descending order
* */
//import java.util.Scanner;

public class BubbleSort {

    public static void main(String[] args) {

        //int counter;  /* hold the size of an array */
        int a;       /*ascending order loop variables (a and b)*/
        int b;

        int i;      /*Descending order loop variables (i and j)*/
        int j;

        int move;    /*Sort variable*/
        //Scanner input = new Scanner(System.in);

       // System.out.println("Enter the total number of integers to sort:");
        //counter = input.nextInt();
       // System.out.println("Enter " + counter + " integers: ");

         int elements[] = {3, 33,2,6, 64, 1};
         //for (i = 0; i < elements.length; i++) {
         //   elements[i] = input.nextInt();
        //}
        /*this for loop sort in  ascending order */
        for (a = 0; a < ((elements.length) - 1); a++)
        {
            for (b = 0; b < (elements.length) - a - 1; b++)
            {
                if (elements[b] > elements[b + 1])
                {
                    move = elements[b];
                    elements[b] = elements[b + 1];
                    elements[b + 1] = move;
                }
            }

        }
        /*  printing out the sorted data in */
        System.out.println("\n[Sorted list of integers in ascending order]");
        System.out.println("--------------------------------------------");
        for (a = 0; a < elements.length; a++) {
            System.out.println(elements[a]);
        }

        /* this for lopp sort in  descending order*/
        for (i = 0; i < ( (elements.length) - 1 ); i++)
        {
            for (j = 0; j < (elements.length) - i - 1; j++)
            {
                if (elements[j] < elements[j+1])
                {
                    move = elements[j];
                    elements[j] = elements[j+1];
                    elements[j+1] = move;
                }
            }
        }
        /* printing out the sorted list in descending order. */
        System.out.println("[Sorted list of integers in descending order]");
        System.out.println("--------------------------------------------");
        for (i = 0; i < elements.length; i++)
            System.out.println(elements[i]);
    }
}
