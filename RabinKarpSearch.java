
import java.io.*;
import java.util.Random;
import java.math.BigInteger;

public class RabinKarpSearch{

    private String pattern;             // String Pattern
    private long sentence;           // pattern hash value
    private int size;                  //pattern length
    private long maxPrime;                //Large prime
    private int radix;                 // radix
    private long radixMarPrime;               //radix^(radixMaxPrime-1) % size

    public RabinKarpSearch(String txt, String stringPattern){

        this.pattern = stringPattern;
        radix = 256;
        size = stringPattern.length();
        maxPrime = longRandomPrime();
        radixMarPrime = 1;

        for (int i = 1; i <= size-1; i++)
            radixMarPrime = (radix * radixMarPrime) % maxPrime;
            sentence = hash(stringPattern, size);

        int position = search(txt);

        if (position == -1) {
            System.out.println("No Match Found");
        }else {
            System.out.println("Pattern found at position : " + position);
        }
    }

    private long hash(String key, int hashRadix){              /** Compute hash **/
        long hash = 0;

        for (int j = 0; j < hashRadix; j++)
            hash = (radix * hash + key.charAt(j)) % maxPrime;

        return hash;
    }

    private boolean check(String txt, int i){                /** Funtion check **/

        for (int j = 0; j < size; j++) {
            if (pattern.charAt(j) != txt.charAt(i + j))
                return false;
        }
        return true;
    }

    private int search(String txt){                       /** Funtion to check for exact match**/

        int N = txt.length();

        if (N < size) return N;

        long txtHash = hash(txt, size);

        if ((sentence == txtHash) && check(txt, 0))                        /** check for match at start **/

        return 0;

        for (int i = size; i < N; i++){                            /** check for hash match. if hash match then check for exact match**/

            // Remove leading digit, add trailing digit, check for match.
            txtHash = (txtHash + maxPrime - radixMarPrime * txt.charAt(i - size) % maxPrime) % maxPrime;
            txtHash = (txtHash * radix + txt.charAt(i)) % maxPrime;

            int offset = i - size + 1;
            if ((sentence == txtHash) && check(txt, offset))
                return offset;
        }
        return -1;
    }

private static long longRandomPrime(){                                 /** generate a random 31 bit prime **/

    BigInteger prime = BigInteger.probablePrime(31, new Random());
    return prime.longValue();
}

public static void main(String[] args) throws IOException {

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    System.out.println("Rabin Karp Algorithm Test\n");
    System.out.println("Enter a sentence");
    String text = br.readLine();

    System.out.println("Enter Pattern");
    String pattern = br.readLine();

    RabinKarpSearch rk = new RabinKarpSearch(text, pattern);
   }
}