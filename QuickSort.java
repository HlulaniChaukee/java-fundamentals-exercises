import java.util.Arrays;


public class QuickSort{
    public static void main(String args[])
    {
        int[] elements ={3, 5, 7, 9,11, 9, 1, 8};

        System.out.println("\nBefore sorting : " + Arrays.toString(elements));
        quickSort(elements);
        System.out.println("After sorting : " + Arrays.toString(elements));
    }

    public static void quickSort(int[] array)
    {
        recursiveQuickSort(array, 0, array.length - 1);
    }

    public static void recursiveQuickSort(int[] array, int startIdx, int endIdx)
    {
        int idx = partition(array, startIdx, endIdx);
        if (startIdx < idx - 1)          // Recursively call quicksort with left part of the partitioned array
        {
        recursiveQuickSort(array, startIdx, idx - 1);
        }

        if (endIdx > idx)               // Recursively call quick sort with right part of the partitioned array
        {
        recursiveQuickSort(array, idx, endIdx);
        }
    }

    public static int partition(int[] array, int left, int right)
    {
        int pivot = array[left];         // taking first element as pivot

        while (left <= right)
        {

            while (array[left] < pivot)      //searching number which is greater than pivot, bottom up
                {
                    left++;
                }
            while (array[right] > pivot)      //searching number which is less than pivot, top down
                 {
                     right--;
                 }

                  if (left <= right)
                  {
                      int tmp = array[left];              // swap the values
                      array[left] = array[right];
                      array[right] = tmp;

                      left++;                   //increment left index and decrement right index
                      right--;
                  }
        }
        return left;
    }
}

