import java.util.Scanner;

public class RbgNumbers{
    public static void main(String []args){
        Scanner add = new Scanner(System.in);
        int r = 0, b = 0, g = 0;

        System.out.println("Enter R value:");
        r = add.nextInt();
        System.out.println("Enter B value:");
        b = add.nextInt();
        System.out.println("Enter G value:");
        g = add.nextInt();

        String value = formatRGB( r, b, g);
        System.out.print("#" +value);
    }

    public static String formatRGB(int r, int g, int b) {
            return (toHexadecimal(r) + toHexadecimal(g) + toHexadecimal(b)).toUpperCase();
        }

    public static String toHexadecimal(int value){

        String digit = Integer.toHexString(value);
        return digit;
    }
}