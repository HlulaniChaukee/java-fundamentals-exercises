import java.text.DecimalFormat;
import java.util.Random;

public class WeightedRandom {
    public static void main(String[] args) {

        String[][] probabilities = { {"apples","0.25"},
                                     {"pears" , "0.5"},
                                     {"grapes" ,"0.2"},
                                     {"oranges","0.05"}};

        //splinting 2-D array
        String[] fruitsNames = new String[probabilities.length];
        double[] weightsValues = new double[probabilities.length];

        weightedRandomValues(fruitsNames, weightsValues, probabilities);
        determinesRandom(weightsValues);
    }

    public static void weightedRandomValues(String [] fruitsNames, double [] weightsValues, String [][] probabilities){

        System.out.println("Weighted random fruits and their values:\n");
        System.out.println("Names\t\t\tvalues\n----------------------\n");

        for(int x = 0;x < probabilities.length;x++) {
            fruitsNames[x] =probabilities[x][0];
            weightsValues[x] += Double.parseDouble(probabilities[x][1]);
            System.out.println(fruitsNames[x] + "\t\t\t" + weightsValues[x]);

        }
        System.out.println();

        double sum = 0;

        System.out.println("Weighted random fruits and their values after cumulative:");
        System.out.println("Names\t\t\tvalues\n----------------------\n");

        for(int x = 0;x < probabilities.length;x++){
            fruitsNames[x] = probabilities[x][0];
            sum += Double.parseDouble(probabilities[x][1]);
            weightsValues[x] = sum;
            System.out.println(fruitsNames[x] + "\t\t\t" + weightsValues[x]);
        }
        System.out.println();
    }

    public static void determinesRandom(double [] weightsValues){

        double start = 0;
        double end = 1;

        double randNumber = new Random().nextDouble();
        double result = start + (randNumber * (end - start));

        DecimalFormat precision = new DecimalFormat("0.00");
        String formant = precision.format(result);
        result = Double.valueOf(formant);

        int i = 0;
        while(i <= weightsValues.length){
            if(result <= weightsValues[i]){
                break;
            }
            i++;
        }
        System.out.println("Weighted random generated value is:   " + result);
        System.out.println("Weighted random number =>             " +weightsValues[i]);
    }
}
