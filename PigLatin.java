import java.util.Scanner;
public class PigLatin {

    public static void main(String[] args) {

        Scanner add = new Scanner(System.in);
        int key ;
        String sentence;

        System.out.println("Enter (1 to translate to pig latin) or (2 to translate to english)");
        key = add.nextInt();

        if(key == 1) {
            System.out.println("Enter word: ");
            sentence = add.next();
            toLatin(sentence);

        }else {
            if(key == 2){
                System.out.println("Enter word without (-way): ");
                sentence = add.next();
                String answer = sentence.replaceAll("-way", "");
               toEnglish(answer);

            }else {
                if(key != 1 && key != 2){
                    System.out.println("Sorry!, wrong option:");
                }
            }
        }
    }

    public static void toLatin(String translateTo ){

        char vowels = Character.toLowerCase(translateTo.charAt(0));

        if (vowels == 'a' || vowels == 'e' || vowels == 'i' || vowels == 'o' || vowels == 'u') {

            String convert = translateTo + "-way";
            System.out.println(convert);
        } else {
            String first = translateTo.substring(0, 1);
            String slice = translateTo.substring(1, translateTo.length());
            System.out.println(slice + first + "-way");
        }
    }

    public static void toEnglish(String translateTo) {
        int length = translateTo.length();

        if (length < 1){
                System.out.print("Enter a word to translate:");
            } else {

            String result = translateTo.charAt(length - 1) + translateTo.substring(0, length - 1);
            System.out.println(result);
        }
    }
}


