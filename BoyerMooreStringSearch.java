import java.util.Scanner;

class BoyerMoore{

    public static void main(String[] args) {
        Scanner add = new Scanner(System.in);

        System.out.println("Enter a sentence:");
        String text = add.next();
        System.out.println("Enter a pattern:");
        String pattern = add.next();

        BoyerMooreStringSearch result = new BoyerMooreStringSearch(pattern);
        int index = result.search(text);
        System.out.println("Match found after position: " +index);
    }
}

public class BoyerMooreStringSearch {
    private final int BASE;
    private int[] text;
    private String pattern;

    public BoyerMooreStringSearch(String pattern) {
        this.BASE = 256;
        this.pattern = pattern;

        text = new int[BASE];
        for (int c = 0; c < BASE; c++)
            text[c] = -1;
        for (int j = 0; j < pattern.length(); j++)
            text[pattern.charAt(j)] = j;
    }

    public int search(String sentence){
        int textSize = sentence.length();
        int patternSize = pattern.length();
        int next = 0;

        for (int i = 0; i <= textSize - patternSize; i += next){
            for (int j = patternSize - 1; j >= 0; j--){
                if (pattern.charAt(j) != sentence.charAt(i+j)){
                    next = Math.max(1, j - text[sentence.charAt(i+j)]);
                    break;
                }
            }
            if (next == 0) return i;
        }
        return textSize;
    }
}


