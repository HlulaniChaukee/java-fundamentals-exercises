
import java.io.*;

public class UnixCommand{

    public static void main(String[] args){

        BufferedReader add = null;
        String fileName = ("C:\\Users\\RALSON\\IdeaProjects\\hluly\\src\\unix.txt");

        try{

            FileReader read = new FileReader(fileName);
            add = new BufferedReader(read);

            countedWordsCharactersLines(fileName, add);

        } catch (IOException ioe) { ioe.printStackTrace(); }

        finally{
            if(add != null){
                try{
                    add.close();

                } catch (IOException ioe) { ioe.printStackTrace(); }
            }
        }
    }

    public static void countedWordsCharactersLines(String fileName, BufferedReader add){

        try{
            String move;
            String[] words;
            int totalWords = 0, lines = 0, characters = 0;
            long totalWords1 = 0;

            while( (move = add.readLine())!= null){
                lines++;
                words = move.split(" ");
                totalWords = totalWords + words.length;
                for(String word: words){
                    characters += word.length();
                    if (word.isEmpty()){
                        totalWords--;
                    }
                }

                // = totalWords + words.length;
                //totalWords1 = totalWords1 + countWords(move);
            }

            System.out.println("Total Lines :\t"+lines);
            System.out.println("Total Characters :\t"+characters);
            System.out.println("Total words :\t" +totalWords);

        }catch(Exception ex){ ex.printStackTrace(); }
    }

    private static long countWords(String line) {

        long numWords = 0;
        int index = 0;
        boolean preventWhitespace = true;

        while (index < line.length()){
            char c = line.charAt(index++);
            boolean currWhitespace = Character.isWhitespace(c);

            if (preventWhitespace || !currWhitespace) {

                numWords++;
            }
            preventWhitespace = currWhitespace;
        }
        return numWords;
    }
}