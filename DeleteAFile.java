/*
* This program create a file called input.txt and directory called docs and delete them.
*
 *  */

import java.io.File;
import java.util.Scanner;

public class DeleteAFile {
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        int key;

        try{
            File newfile = new File("C:\\Users\\RALSON\\IdeaProjects\\hluly\\src\\input.txt"); //create file called input.txt
            newfile.createNewFile();

            File directory = new File("C:\\Users\\RALSON\\IdeaProjects\\hluly\\src\\docs");   //create a directory called docs
            directory.mkdir();

            if(newfile.exists() && directory.exists()){

                System.out.println("File\t" +newfile.getName()+"\tand directory\t" +directory.getName()+"\twas created");

            } else {

                System.out.println("Files does not exist");
            }
               System.out.println("Enter the value (1 to delete a file) or (2 to delete a directory) or (0 to exist)");
               key = input.nextInt();

            if (key !=1 && key != 2) {
                System.out.println("Invalid entry, try again next time.");
            }

            if(key == 1) {
                newfile.deleteOnExit();

                if (newfile.delete()) {
                    System.out.println(newfile.getName() + "\t file deleted");
                } else {
                    System.out.println("Delete operation is failed.");
                }
            } else {
                if (key == 2) {
                    directory.deleteOnExit();

                    if (directory.delete()) {
                        System.out.println(directory.getName() + "\t directory deleted");
                    } else {
                        System.out.println("Delete operation is failed.");
                    }
                }
            }
        }
        catch(Exception e) { e.printStackTrace();}
    }
}




