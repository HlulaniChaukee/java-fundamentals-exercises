import java.io.*;

public class WalkTheDirectoryTree {
    public static void main(String[] args) {

        WalkTheDirectoryTree search = new WalkTheDirectoryTree();
        search.walk("C:\\Users\\RALSON\\IdeaProjects\\hluly\\src\\" );
    }

    public void walk( String treePath )
    {
        File root = new File( treePath );

        File[] list = root.listFiles(new FileFilter(){
                                     public boolean accept(File file){
                                     return file.isDirectory() || file.getName().toLowerCase().endsWith(".txt");
                                     }});

        if (list == null) return;

        for (File f : list) {
            if ( f.isDirectory() )
            {
                walk( f.getAbsolutePath() );
                System.out.println( "Dir:" + f.getAbsoluteFile() );
            }
            else
            {
                System.out.println( "File:" + f.getAbsoluteFile() );
            }
        }
    }
}