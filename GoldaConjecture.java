import java.util.Scanner;

public class GoldaConjecture {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int number;
        int i;

        System.out.println("Enter an even greater than 2 to find it combination");
        number = scan.nextInt();

        if(number%2 != 0 || number ==2){
            System.out.println("Invalid input");

        } else {

            for(i = 1; i <Math.sqrt(number); i += 2)
            {
                if(isPrime(i) && isPrime(number - i))
                {
                    System.out.println("Values:\t"+ i + " + "+(number-i)+ " = " +number);

                    i = number/2;                      //terminating the printed values
                }
            }
        }
    }

    public static boolean isPrime(int value)
    {
        int count;
        if(value%2 == 0 || value ==2)
        {
            return false;
        }
        for(count = 2; 2*count < value; count++ )
        {
            if(value%count ==0)
            {
                return false;
            }
        }
        return true;
    }
}