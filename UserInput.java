/**
 * This program accept a string and  the integer 75000 from the text console.
 */

import java.io.PrintStream;
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner add = new Scanner(System.in);
        int number = 0;
        String name;

        System.out.println("Enter a String:");
        name = add.next();
        System.out.println("Enter an integer (75000)");
        number = add.nextInt();

        System.out.println("Your string is:\t" + name);
        System.out.println("The value is: \t" + number);
    }
}
