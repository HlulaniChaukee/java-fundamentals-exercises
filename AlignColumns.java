
public class AlignColumns {

    public static void main(String[] args) {
        String list[] =
                {
                        "Given$a$text$file$of$many$lines$where$fields$within$a$line",
                        "are$delineated$by$a$single$'dollar'$character$write$a$program",
                        "that$aligns$each$column$of$fields$by$ensuring$that$words$in$each",
                        "column$are$separated$by$at$least$one$space.",
                        "Further$allow$for$each$word$in$a$column$to$be$either$left",
                        "justified$right$justified$or$center$justified$within$its$column."
                };
        String[] align;
        String massege = "";

        System.out.println("-----------------------\nLeft justfied clolums\n-------------------------");
        for (String info : list) {
            align = info.split("\\$");

            for (String l : align) {
                String line = String.format("%-12s", l);
                System.out.print(line);
            }
            System.out.println();

        }
        System.out.println();
        System.out.println("--------------------\nRight justfied\n--------------------");
        for (String info2 : list) {
            align = info2.split("\\$");

            for (String r : align) {
                String line2 = String.format("%12s",r);
                System.out.printf(line2);
            }
            System.out.println();
        }

        System.out.println();
        System.out.println("--------------------\nCentre justfied\n--------------------");
        for (String info2 : list) {
            align = info2.split("\\$");

            for (String c : align) {
                alignCenter(c);
            }
            System.out.println();
        }
    }

    public static void alignLeft(String text){

    }
    public static void alignCenter(String text) {

        int width = 15;
        int padSize = width - text.length();
        int padStart = text.length() + padSize / 2;

        text = String.format("%" + padStart + "s", text);
        text = String.format("%-" + width  + "s", text);
        System.out.print(text);
    }
}
