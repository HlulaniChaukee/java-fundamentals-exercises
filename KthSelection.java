
public class KthSelection {
    public static int getKthMinimum(int[] inputArray, int k) {
        return findKthMinimum(inputArray, k)[k - 1];
    }
    public static int[] getMinimumKSortedElements(int[] inputArray, int k) {
        return findKthMinimum(inputArray, k);
    }
    private static int[] findKthMinimum(int[] inputArray, int k) {
        int[] partiallySorted = new int[k];
        int[][] outputTree = getOutputTree(inputArray);
        int root = getRootElement(outputTree);
        partiallySorted[0] = root;
        int rootIndex = 0;
        int level = outputTree.length;
        int[][][] fullAdjacencyList = new int[k - 1][][];
        int[] kthMinIdx = null;
        for (int i = 1; i < k; i++) {
            fullAdjacencyList[i - 1] = getAdjacencyList(outputTree, root,
                    level, rootIndex);
            kthMinIdx = getKthMinimum(fullAdjacencyList, i, root);
            int row = kthMinIdx[0];
            int column = kthMinIdx[1];
            root = fullAdjacencyList[row][column][0];
            partiallySorted[i] = root;
            level = column + 1;
            rootIndex = fullAdjacencyList[row][column][1];
        }

        return partiallySorted;
    }

    public static int[][] getOutputTree(int[] values) {
        Integer size = new Integer(values.length);
        double treeDepth = Math.log(size.doubleValue()) / Math.log(2);
        // int intTreeDepth = getIntValue(Math.ceil(treeDepth)) + 1;
        int intTreeDepth = (int) (Math.ceil(treeDepth)) + 1;
        int[][] outputTree = new int[intTreeDepth][];

        // first row is the input
        outputTree[0] = values;
        printRow(outputTree[0]);

        int[] currentRow = values;
        int[] nextRow = null;
        for (int i = 1; i < intTreeDepth; i++) {
            nextRow = getNextRow(currentRow);
            outputTree[i] = nextRow;
            currentRow = nextRow;
            printRow(outputTree[i]);
        }
        return outputTree;
    }

    private static int[] getNextRow(int[] values) {
        int rowSize = getNextRowSize(values);
        int[] row = new int[rowSize];
        int i = 0;
        for (int j = 0; j < values.length; j++) {
            if (j == (values.length - 1)) {
                // this is the case where there are odd number of elements
                // in the array. Hence the last loop will have only one element.
                row[i++] = values[j];
            } else {
                row[i++] = getMin(values[j], values[++j]);
            }
        }
        return row;
    }

    private static int[] getKthMinimum(int[][][] fullAdjacencyList, int kth,
                                       int kMinusOneMin) {
        int kThMin = Integer.MAX_VALUE;
        int[] minIndex = new int[2];
        int j = 0, k = 0;
        int temp = -1;

        for (int i = 0; i < kth; i++) {
            for (j = 0; j < fullAdjacencyList.length; j++) {
                int[][] row = fullAdjacencyList[j];
                if (row != null) {
                    for (k = 0; k < fullAdjacencyList[j].length; k++) {
                        temp = fullAdjacencyList[j][k][0];
                        if (temp <= kMinusOneMin) {
                            continue;
                        }
                        if ((temp > kMinusOneMin) && (temp < kThMin)) {
                            kThMin = temp;
                            minIndex[0] = j;
                            minIndex[1] = k;
                        }
                    }
                }
            }
        }
        return minIndex;
    }

    public static int[][] getAdjacencyList(int[][] tree, int rootElement,
                                           int level, int rootIndex) {
        int[][] adjacencyList = new int[level - 1][2];
        int adjacentleftElement = -1, adjacentRightElement = -1;
        int adjacentleftIndex = -1, adjacentRightIndex = -1;
        int[] rowAbove = null;

        // we have to scan in reverse order
        for (int i = level - 1; i > 0; i--) {
            // one row above
            rowAbove = tree[i - 1];
            adjacentleftIndex = rootIndex * 2;
            adjacentleftElement = rowAbove[adjacentleftIndex];

            // the root element could be the last element carried from row above
            // because of odd number of elements in array, you need to do
            // following
            // check. if you don't, this case will blow {8, 4, 5, 6, 1, 2}
            if (rowAbove.length >= ((adjacentleftIndex + 1) + 1)) {
                adjacentRightIndex = adjacentleftIndex + 1;
                adjacentRightElement = rowAbove[adjacentRightIndex];
            } else {
                adjacentRightElement = -1;
            }

            // if there is no right adjacent value, then adjacent left must be
            // root continue the loop.
            if (adjacentRightElement == -1) {
                // just checking for error condition
                if (adjacentleftElement != rootElement) {
                    throw new RuntimeException(
                            "This is error condition. Since there "
                                    + " is only one adjacent element (last element), "
                                    + " it must be root element");
                } else {
                    rootIndex = rootIndex * 2;
                    adjacencyList[level - 1][0] = -1;
                    adjacencyList[level - 1][1] = -1;
                    continue;
                }
            }

            // one of the adjacent number must be root (min value).
            // Get the other number and compared with second min so far
            if (adjacentleftElement == rootElement
                    && adjacentRightElement != rootElement) {
                rootIndex = rootIndex * 2;
                adjacencyList[i - 1][0] = adjacentRightElement;
                adjacencyList[i - 1][1] = rootIndex + 1;
            } else if (adjacentleftElement != rootElement
                    && adjacentRightElement == rootElement) {
                rootIndex = rootIndex * 2 + 1;
                adjacencyList[i - 1][0] = adjacentleftElement;
                adjacencyList[i - 1][1] = rootIndex - 1;
            } else if (adjacentleftElement == rootElement
                    && adjacentRightElement == rootElement) {
                // This is case where the root element is repeating, we are not
                // handling this case.
                throw new RuntimeException(
                        "Duplicate Elements. This code assumes no repeating elements in the input array");
            } else {
                throw new RuntimeException(
                        "This is error condition. One of the adjacent "
                                + "elements must be root element");
            }
        }

        return adjacencyList;
    }

    private static int getMin(int num1, int num2) {
        return Math.min(num1, num2);
    }

    private static int getNextRowSize(int[] values) {
        return (int) Math.ceil(values.length / 2.0);
    }

    public static int getRootElement(int[][] tree) {
        int depth = tree.length;
        return tree[depth - 1][0];
    }

    private static void printRow(int[] values) {
        for (int i : values) {
            // System.out.print(i + " ");
        }
    }

    public static void main(String args[]) {
        int[] input = { 2, 14, 5, 13, 1, 8, 17, 10, 6, 12, 9, 4, 11, 15, 3, 16 };
        System.out.println("Fifth Minimum: " + getKthMinimum(input, 5));

        int minimumSortedElementSize = 10;
        int[] tenMinimum = getMinimumKSortedElements(input,
                minimumSortedElementSize);
        System.out.print("Minimum " + minimumSortedElementSize + " Sorted: ");
        for (int i = 0; i < minimumSortedElementSize; i++) {
            System.out.print(tenMinimum[i] + " ");
        }
    }
}
