import java.util.Scanner;

public class MorseCode {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
      int key = 0;

        String[] alpha = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8",
                "9", "0", " "};
        String[] morse = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
                "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.",
                "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-",
                "-.--", "--..", ".----", "..---", "...--", "....-", ".....",
                "-....", "--...", "---..", "----.", "-----", " "};


        System.out.println("Enter 1 (cconvert to morse code) or (Convert to alphanumerical)");
        key = scan.nextInt();
        if (key == 1){

            System.out.println("Enter a alphanumeric sentence:");
            String word = scan.next();
            String convertedInfo = convertToMorse(alpha, morse, word);
            System.out.println(convertedInfo);


        } else {
            if (key == 2) {

                System.out.println("Enter a morse code");
                String code = scan.next();
                String convertedInfo = convertToAlphanumeric(alpha, morse, code);
                System.out.println(convertedInfo);

            } else {
                System.out.println("Enter an invalid choice ( 1 or 2)");
            }
        }
    }

    public static String convertToMorse(String[] alpha, String[] morse, String word) {
        String sentence = "";
        for (int x = 0; x < word.length(); ++x) {
            for (int y = 0; y < alpha.length; y++) {
                if (String.valueOf(word.charAt(x)).equalsIgnoreCase(alpha[y])) {
                    sentence = sentence + morse[y] + " ";
                }
            }
        }
        return sentence;
    }

    public static String convertToAlphanumeric(String[] alpha, String[] dottie, String word) {
        String code = word.replaceAll("  ", "#");
        String stringCode = "";

        String[] morseCode = code.split("#");
        for (String mCode : morseCode) {
            String[] msCode = mCode.split(" ");
            for (String mosCode : msCode) {
                for (int y = 0; y < dottie.length; y++) {

                    if (mosCode.equals(dottie[y])) {
                        stringCode = stringCode + alpha[y];
                    }
                }
            }
            stringCode += " ";
        }
        return stringCode;
    }
}