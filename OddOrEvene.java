import java.util.Scanner;

public class OddOrEvene {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int i = 0;

        System.out.println("Enter an integer");
        i = input.nextInt();

        if (i % 2 == 0) {
            System.out.println("You have entered an even number\n");
        } else {
            System.out.println("You have entered an odd number\n");
        }

           System.out.println("Using bit Operator &");
        if (isEven(i)){
            System.out.println("Is even");
        } else {
            System.out.println("Is odd");
        }
    }

    public static boolean isEven(int x) {

        if ((x & 1) == 0) {
            return true;
        } else {
            return false;
        }
    }
}
