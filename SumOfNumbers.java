
/*
* This program ask the user to enter an integer four times.
* It uses a for loop.
*
* Sum of numbers exercise.
*/

public class SumOfNumbers {
    public static void main(String []args){

        int values[] = {2, 45, 12, 2, 7, 9};
        int sum = 0;
        int i = 0;

        for(int count =0; count < values.length; count++) {
            i++;

            System.out.println(values[count]);
            sum += values[count];
        }
        System.out.print("==========\n Sum is:" +sum+ "\n==========");   //printing the sum
    }
}
