/*
* These program print out the number of beers on the wall..
* It reduces the sum of bottles by 1 until there are no beers on the wall.
* */

public class BottlesOfBeers
{

    public static void main(String[] args)
    {
        int bottle = 99;
        while (bottle >= 0)
        {
            System.out.println(bottle + "\t bottles of beers on the wall");
            System.out.println(bottle + "\t bottles of beer");

            if(bottle != 0)
            {
                System.out.println("Take one down, pass it around\n");
            }
            bottle--;
        }
    }
}

