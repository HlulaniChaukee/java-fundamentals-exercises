

public class GameOfLife{
    public static void main(String[] args){
        String[] dish= {"_#_", "_#_", "_#_",};
        int result= 3;
        for(int i= 0;i < result;i++){
            System.out.println("result " + i + ":");
            print(dish);
            dish = lifeGame(dish);
        }
    }

    public static String[] lifeGame(String[] dish){
        String[] newResult= new String[dish.length];
        for(int row= 0;row < dish.length;row++){
            newResult[row]= "";
            for(int i= 0;i < dish[row].length();i++){
                String above= "";
                String same= "";
                String below= "";
                if(i == 0){
                    above = (row == 0) ? null : dish[row - 1].substring(i, i + 2);
                    same= dish[row].substring(i + 1, i + 2);

                    below= (row == dish.length - 1) ? null : dish[row + 1].substring(i, i + 2);
                }else if(i == dish[row].length() - 1){//right

                    above = (row == 0) ? null : dish[row - 1].substring(i - 1, i + 1);
                    same = dish[row].substring(i - 1, i);
                    below = (row == dish.length - 1) ? null : dish[row + 1].substring(i - 1, i + 1);
                }else{
                    above= (row == 0) ? null : dish[row - 1].substring(i - 1, i + 2);
                    same= dish[row].substring(i - 1, i) + dish[row].substring(i + 1, i + 2);

                    below= (row == dish.length - 1) ? null : dish[row + 1].substring(i - 1, i + 2);
                }
                int neighbors= getNeighbors(above, same, below);
                if(neighbors < 2 || neighbors > 3){ newResult[row]+= "_";

                }else if(neighbors == 3){ newResult[row]+= "#";

                }else{ newResult[row]+= dish[row].charAt(i);
                }
            }
        }
        return newResult;
    }

    public static int getNeighbors(String above, String same, String below){
        int answer = 0;
        if(above != null){
            for(char x: above.toCharArray()){
                if(x == '#') answer++;
            }
        }
        for(char x: same.toCharArray()){
            if(x == '#') answer++;
        }
        if(below != null){
            for(char x: below.toCharArray()){
                if(x == '#') answer++;
            }
        }
        return answer;
    }

    public static void print(String[] dish){
        for(String s: dish){
            System.out.println(s);
        }
    }
}