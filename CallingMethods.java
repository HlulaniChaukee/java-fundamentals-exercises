
public class CallingMethods extends PrivateProtectedPublic {

    public static void main(String[] args) {

        /*******************************************
         Calling a method that requires no arguments
         *******************************************/
        noArgumentMethod();

        /*************************************************
         Calling a method with a fixed number of arguments
         **************************************************/
        fixedArgumentsMethod(10, 10.5);

        /****************************************
         Calling a method with optional arguments
         *****************************************/
        int number = 0;
        System.out.println("Optional arguments:  " + optionalArguments(1, 4, 5, 2, 7));

        /****************************************************
         Calling a method with a variable number of arguments
         ****************************************************/
        double result = variableArgumentsMethod(2, 4, 7, 23, 45, 23);
        System.out.println("From variable arguments largest is:  " + result);

        /**************************************
         Obtaining the return value of a method
         **************************************/
        int length = 5, height = 3;
        int answer = area(length, height);
        System.out.println("Returned value from method area:  " + answer);

        /************************************************************
         Stating whether arguments are passed by value or by reference
         ***************************************************************/
        //By value
        String name = null;
        byValue(name);

        //By reference
        Animal animal = new Animal("Tiger");
        System.out.println("Before reference:  " + animal.name);
        byReference(animal);

        /*************************************
         * Private, protected and public methods
         *************************************/
        CallingMethods value = new CallingMethods();
        value.result();
    }


    /***********************************
     * method that requires no arguments
     ************************************/
    public static void noArgumentMethod() {
        System.out.println("Returned by method that requires no arguments  ");
    }

    /***********************************************
     * method that require fixed number of arguments
     ***********************************************/
    public static void fixedArgumentsMethod(int a, double b) {
        System.out.println("From fixed arguments:  " +(a * b));
    }

    /*********************************
     * method with optional arguments
     *********************************/
    public static int optionalArguments(int... nums) {
        int total = 0;
        for (int count : nums) {
            total += count;
        }
        return total;
    }

    /******************************************
     * method with variable number of arguments
     ****************************************/
    public static double variableArgumentsMethod(double... values) {
        double largest = Double.MIN_VALUE;
        for (double v : values)
            if (v > largest)
                largest = v;
        return largest;
    }

    /****************************************
     * Obtaining a return value from a method
     ****************************************/
    public static int area(int length, int height) {
        return length * height;
    }

    /****************
     * Pass by value
     *************/
    public static void byValue(String name) {
        name = " Dog";
        System.out.println("Passed by value:  " + name);
    }

    /*******************
     * Pass by reference
     *******************/
    public static void byReference(Animal animal) {
        animal.name = "Lion ";
        System.out.println("Passed by reference:  " + animal.name);
    }

    private static class Animal {
        private String name;

        public Animal(String name) {
            this.name = name;
        }
    }
    /***************************************
     * Private, protected and public method
     ***************************************/
    void  result(){
        PrivateProtectedPublic add = new PrivateProtectedPublic();
        add.getString();
        add.total();
    }

}
    /***************************************
     * Private, protected and public class
     ***************************************/
    class PrivateProtectedPublic {

        private String sentence(){
            String text = "From private method  ";
            return text;
        }
        public String getString() {
            return sentence();            //calling private on public
        }
        protected static void total()
        {
            System.out.println("From protected method:");
        }
    }

