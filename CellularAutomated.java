public class CellularAutomated{
    public static void main(String[] args) throws Exception{
        String start= "_xxx_xxx_xxx_xxx_xxx_xxx_x ";
        int numGens = 10;
        for(int i = 0; i < numGens; i++){
            System.out.println("Step " + i + ":   " + start);
            start = life(start);
        }
    }

    public static String life(String lastGen){
        String newStep= "";
        for(int i = 0; i < lastGen.length(); i++){
            int neighbors= 0;
            if (i == 0){//left edge
                neighbors = lastGen.charAt(1) == 'x' ? 1 : 0;
            } else if (i == lastGen.length() - 1){//right edge
                neighbors = lastGen.charAt(i - 1) == 'x' ? 1 : 0;
            } else{//middle
                neighbors= getNeighbors(lastGen.substring(i - 1, i + 2));
            }

            if (neighbors == 0){//dies or stays dead with no neighbors
                newStep+= "_";
            }
            if (neighbors == 1){//stays with one neighbor
                newStep+= lastGen.charAt(i);
            }
            if (neighbors == 2){//flips with two neighbors
                newStep+= lastGen.charAt(i) == 'x' ? "_" : "x";
            }
        }
        return newStep;
    }

    public static int getNeighbors(String group){
        int ans= 0;
        if (group.charAt(0) == 'x') ans++;
        if (group.charAt(2) == 'x') ans++;
        return ans;
    }
}