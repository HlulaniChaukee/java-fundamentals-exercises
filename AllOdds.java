/*
* The program print odd numbers between 1-99.
* The program check if a number is odd before printing.
* */
public class AllOdds {
    public static void main(String []args) {
        int number = 0;
        odds(number);
    }

    public static void odds(int x)
    {
        for (int i = 1; i <= 99; i++)
        {
            x++;
            if(x%2 != 0)
            {
                System.out.println(x);
            }
        }

    }
}
