import java.util.Scanner;

public class BruteForceStringSearch{

    private char[] text;
    private char[] pattern;
    private int textSize;
    private int patternSize;

    public void setString(String txt, String pat){
        this.text = txt.toCharArray();
        this.pattern = pat.toCharArray();
        this.textSize = txt.length();
        this.patternSize = pat.length();
    }

    public int search(){
        for (int i = 0; i < textSize - patternSize; i++){
            int j = 0;
            while (j < patternSize && text[i+j] == pattern[j]){
                j++;
            } if (j == patternSize) return i;
        }
        return -1;
    }
}

class BruteForce {
    public static void main(String[] args) {

        Scanner add = new Scanner(System.in);
        BruteForceStringSearch bfs = new BruteForceStringSearch();
        System.out.println("Enter A Sentence:");
        String text = add.nextLine();
        System.out.println("Enter a pattern to search:");
        String pattern = add.nextLine();

        bfs.setString(text, pattern);
        int first_occur_position = bfs.search();
        System.out.println("The text '" + pattern + "' is first found after the " + first_occur_position + " position.");
    }
}

