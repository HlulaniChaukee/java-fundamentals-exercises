import java.util.Random;

public class BalancingBrackets {
    public static void main(String[] args){

        String[] bracktString = {"", "[]", "][", "[][]", "][][", "[[][]]", "[]][[]"};

        for(int count = 0; count <= 16; count += 2){

            String bracks = generateBrackets(count);
            System.out.println(bracks + ": " + checkBrackets(bracks));
        }

        for(String test: bracktString){
            System.out.println(test + ": " + checkBrackets(test));
        }
    }

    public static boolean checkBrackets(String str){
        int noMatch = 0;
        for(char character:str.toCharArray()){
            if(character == '['){
                noMatch++;
            }else if(character == ']'){
                noMatch--;
            }else{
                return false;
            }
            if(noMatch < 0){
                return false;
            }
        }
        return noMatch == 0;
    }

    public static String generateBrackets(int number){
        Random rand = new Random();
        if(number % 2 == 1){
            return null;
        }

        String result = "";

        int leftBracket = number / 2;
        int opened = 0;
        while(number > result.length()){
            if(Math.random() >= .5 && leftBracket > 0 || opened == 0){
                result += '[';
                leftBracket--;
                opened++;
            }else{
                result += ']';
                opened--;
            }
        }
        return result;
    }
}