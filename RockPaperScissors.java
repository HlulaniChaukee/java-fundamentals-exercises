/*
* Rock, Papers and Scissors by Chauke Hlulani Ralson
*
* This program randomly guess a name (rock, paper, scissors)
* and compare it with user's choice.
* It keep a history of uesr's choice, to properly generate a name at the next stage.
* */

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        Scanner add = new Scanner(System.in);

        ArrayList<String> userRecord = new ArrayList<>();

        String machine = null;  //Players
        String operator;

        String option = "y";

        do {
            machine = randomNumber();
            operator = guessValue(userRecord);
            determineWinner(machine, operator);
            userRecord.add(operator);     //adding user's choice to the array

            System.out.println();
        }
        while (option.equalsIgnoreCase(machine));
    }//end of main

    public static String matchValue(int guess) {  //initialise values to be guessed by pkayers

        String choice;
        switch (guess) {
            case 1:
                choice = "rock";
                break;
            case 2:
                choice = "paper";
                break;
            case 3:
                choice = "scissors";
                break;
            default:
                choice = null;
        }
        return choice;
    }

    public static String randomNumber() {     //returns value to method matchValue
        Random rand = new Random();
        int randomizedNumber = rand.nextInt(3) + 1;

        return matchValue(randomizedNumber);
    }


    public static String guessValue( ArrayList<String> userRecord) {   //get value and returns it to matchValue

        Scanner add = new Scanner(System.in);
        int guess = 0;
        String play = "";

        if (userRecord.isEmpty()) {
            System.out.println("Enter (1 rock), (2 paper), or (3 scissors)");
            guess = add.nextInt();
            play = matchValue(guess);

        }else {
            while (play == null) {
                System.out.println("Enter (1 rock), (2 paper), or (3 scissors) ");
                guess = add.nextInt();
                play = matchValue(guess);
            }

        }
        return play;
    }

    public static void determineWinner(String computerChoice, String userChoice) {  //determines the winner

        System.out.print(" Machine's choice is:  '" + computerChoice + "'.\n");
        System.out.print(" User's choice is      '" + userChoice + "'.\n\n");

        if (userChoice.equalsIgnoreCase("rock")) {
            if (computerChoice.equalsIgnoreCase("scissors")) {
                System.out.print("Rock beats scissors.\nCongratulation you won!");

            } else if (computerChoice.equalsIgnoreCase("paper")) {
                System.out.print("Paper beats rock.\nThe machine wins, you lost!");

            } else {
                System.out.print("The game is tied!\nPlay again.......");
            }
        } else if (userChoice.equalsIgnoreCase("paper")) {
            if (computerChoice.equalsIgnoreCase("scissors")) {
                System.out.println("Scissors beats paper.\nThe machine wins, you lost");

            } else if (computerChoice.equalsIgnoreCase("rock")) {
                System.out.print("Paper beats rock.\nCongratulation you won! ");

            } else {
                System.out.print("The game is tied!\nPlay again.......");
            }

        } else if (userChoice.equalsIgnoreCase("scissors")) {
            if (computerChoice.equalsIgnoreCase("rock")) {
                System.out.print("Rock beats scissors.\nThe machine wins, you lost!");

            } else if (computerChoice.equalsIgnoreCase("paper")) {
                System.out.print("Scissors beats paper.\nCongratulation you won!");

            } else {
                System.out.print("The game is tied!\nPlay again.......");
            }
        }
    }
}