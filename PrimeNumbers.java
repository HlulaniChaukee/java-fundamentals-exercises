import java.util.Scanner;

public class PrimeNumbers {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int maxNumber = 0;
        int digit = 0;

        System.out.println("Please enter the nth prime number you want to find: ");
        maxNumber = input.nextInt();
        myPrimes(maxNumber);
    }

    public static void myPrimes(int maximum ){

        int primeCounter = 0;
        for(int count = 2, prime = 2; count <= maximum; count++){
            for(prime = 2; prime < count; prime++){
                if(count % prime == 0){
                    break;
                }
            } if(prime == count){
                primeCounter++;
                System.out.print(prime+ "  ");
            }
        }
        System.out.println("\nThere are: " +primeCounter+ " prime numbers");
    }
}