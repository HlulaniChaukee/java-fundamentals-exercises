
public class FindTheMaximum {

    public static void main(String []args) {

        int [] values = {5, 9, 2, 12, 101,99};
        int largest = values[0];

        for (int count = 1; count < values.length; count++)
        {
            if (largest < values[count])
            {
                largest = values[count];
            }
        }
        System.out.println(largest);
    }
}
