import java.text.SimpleDateFormat;
import java.util.Date;

public class SystemTime {
    public static void main(String []args){

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        System.out.println(strDate);
    }
}
